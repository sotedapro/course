# Julkaisusuunnitelma


```mermaid
gantt
    title Julkaisusuunnitelma
    dateFormat  DD-MM-YYYY
    section Julkaisut
    Feature 1  v 1.0       :active,v 1.0, 25-01-2019, 02-04-2019
    Feature 2  v 1.0       :active,v 1.0, 15-01-2019, 20-03-2019
    Feature 3  v 1.1       :active,v 1.0, 01-02-2019, 02-04-2019
    Feature 4  v 1.1       :active,v 1.0, 30-03-2019, 02-04-2019
    Feature 5  v 2.3       :active,v 1.0, 21-01-2019, 15-03-2019
    Feature 6  v 0.9       :active,v 1.0, 21-02-2019, 27-03-2019
    Feature 7  v 1.1       :active,v 1.0, 21-02-2019, 01-04-2019

```




**Julkaisu "EarlyAdopter"**

Versio 1.0

**Konfiguraatio**

| Ominaisuus | Versio | Testattavissa | Julkaistaan |
|:-:|:-:|:-:|:-:|
| [Feature 1]() | 1.0 | x.y.201z | x+2,y+3.201z |
| [Feature 2]() | 1.0 | x.y.201z | x+2,y+3.201z |
| [Feature 3]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 4]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 5]() | 2.3 | x.y.201z | x+2,y+3.201z |
| [Feature 6]() | 0.9 | x.y.201z | x+2,y+3.201z |
| [Feature 7]() | 1.1 | x.y.201z | x+2,y+3.201z |




**Julkaisu "EarlyAdopter Enhanced"**

Versio 1.1

**Konfiguraatio**

| Ominaisuus | Versio | Testattavissa | Julkaistaan |
|:-:|:-:|:-:|:-:|
| [Feature 1]() | 1.0 | x.y.201z | x+2,y+3.201z |
| [Feature 2]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 3]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 4]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 5]() | 2.5 | x.y.201z | x+2,y+3.201z |
| [Feature 6]() | 0.9 | x.y.201z | x+2,y+3.201z |
| [Feature 7]() | 1.2 | x.y.201z | x+2,y+3.201z |


**Julkaisu "EarlyAdopter Enhanced and stabilized"**

Versio 1.2

**Konfiguraatio**

| Ominaisuus | Versio | Testattavissa | Julkaistaan |
|:-:|:-:|:-:|:-:|
| [Feature 1]() | 1.0 | x.y.201z | x+2,y+3.201z |
| [Feature 2]() | 1.2 | x.y.201z | x+2,y+3.201z |
| [Feature 3]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 4]() | 1.1 | x.y.201z | x+2,y+3.201z |
| [Feature 5]() | 2.5 | x.y.201z | x+2,y+3.201z |
| [Feature 6]() | 0.9 | x.y.201z | x+2,y+3.201z |
| [Feature 7]() | 1.3 | x.y.201z | x+2,y+3.201z |
