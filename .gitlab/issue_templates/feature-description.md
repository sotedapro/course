# Feature XXXXXXX

* Responsible person: Marko Polo
* Status (Accepted/Postponed/Rejected etc)

### Description

Sed non nisi id ligula interdum mattis. Fusce vel ullamcorper nunc. Nulla pharetra dui ut enim semper semper. Aenean ut leo tortor. Fusce felis nibh, malesuada vitae nibh at, rhoncus feugiat leo. Nam vehicula vitae ligula vitae condimentum. Ut maximus metus nec lorem ultrices elementum.


### Affected Use Cases (Use Cases)

Linked Use Cases

* [Use Case 1](FT1-kayttotapaus.md)
* [Use Case 2](FT2-kayttotapaus.md)


### User Storys (User Storys)

* _Käyttäjänä haluan, että lorem ipsum on näkyvillä koko käytön ajan, koska se tuo minulle turvallisen olon_
* _Ylläpitäjänä haluan, että tervehdysviesti on vaihdettavissa, koska se virkistää työntekoa!_

Link to real User Storys

- [ ] #2
- [ ] #6
- [ ] #5






### User Interface Mockup/Design

![](https://openclipart.org/image/300px/svg_to_png/178764/1370010418.png&disposition=attachment)


### Functional Requirements

**Esim**

* Käyttäjä näkee jatkuvasti ruudulla "Lorem Ipsum"-viestin
* Tervehdysviesti on pääkäyttäjän muokattavissa

### Non-Functional Requirements


* Usability: Uuden tervehdysviestin generointi saa kestää < 0.5 s
* Security:
* Scalability:
* 


### Restrictions 

* Al
* 


### Test Cases for Feature

| Test Case  | Source for test  | Who is responsible  |
|:-: | :-:|:-:|
| [Test Case 1]( FT1-testitapaus1.md)  | Use Case 1  |  |
| [Test Case 2]( FT1-testitapaus2.md)  |  |  |
| [Test Case 3]( FT1-testitapaus3.md)  | Use Case 2 |  |
| [Test Case 4]( FT1-testitapaus4.md)  | Vaatimus REQ001 |  |
| [Testitapaus 5]( FT1-testitapaus5.md)  |  |  |
| [Testitapaus 6]( FT1-testitapaus6.md)  |  |  |


### Schedule/Roadmap

| Status | |
|:----:|:----:|
| Accepted/ Rejected | 1.1.2014 |

/label ~Feature


